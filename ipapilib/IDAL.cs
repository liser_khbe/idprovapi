﻿using System;

namespace ipapilib
{
    public interface IDAL
    {
        // TODO : write test for the overloads with Action<T> callbacks
        /* GET   : /v1/customer/{login}
         * GET   : /v1/customer/login/{login} */
        Customer GetCustomerByLogin(string login);
        void GetCustomerByLoginAsync<T>(string login, Action<T> callback);

        /* GET   : /v1/customer/uid/{uid} */
        Customer GetCustomerByUID(string uid);
        void GetCustomerByUIDAsync<T>(string uid, Action<T> callback);
        
        /* POST  : /v1/customer */
        string CreateCustomer(Customer customer);
        void CreateCustomerAsync<T>(Customer customer, Action<T> callback);

        /* PUT   : /v1/customer */
        void UpdateCustomer(Customer customer);
        void UpdateCustomerAsync<T>(Customer customer, Action<T> callback);

        /* PATCH : /v1/customer/{login}/{uid} */
        void SetCustomerUID(string login, string uid);
        void SetCustomerUIDAsync<T>(string login, string uid, Action<T> callback);

        /* DELETE: /v1/customer/{login} */
        void DeleteCustomer(string login);
        void DeleteCustomerAsync<T>(string login, Action<T> callback);
    }
}
