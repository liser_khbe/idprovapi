﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using ipapilib.utils;

namespace ipapilib {
    /// <summary>
    /// Customer model
    /// </summary>
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Customer : IEquatable<Customer> {
        public virtual string Login { get; set; }
        public virtual string CardUID { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string SSN { get; set; }
        [JsonConverter(typeof(CustomeDateTimeConverter))]
        public virtual DateTime? DateOfBirth { get; set; }
        public virtual AffiliationType AffiliationType { get; set; }

        /// <summary>
        /// Parameterless Constructor for Serialization.
        /// </summary>
        public Customer() { }
        public Customer(string lastName, string firstName, string emailAddress, AffiliationType affiliationType, string cardUID, string login = null, string ssn = null, DateTime? dateOfBirth = null) {
            if (!cardUID.Length.In(8, 14, 20))
                throw new ArgumentException("Incorrect CardUID length (4, 7 or 10 Bytes i.e. 8,14,20 hex characters)");

            Login = login;
            CardUID = cardUID;
            LastName = lastName;
            FirstName = firstName;
            EmailAddress = emailAddress;
            SSN = ssn;
            DateOfBirth = dateOfBirth;
            AffiliationType = affiliationType;
        }

        public override bool Equals(object other) {
            return this.Equals(other as Customer);
        }

        public override int GetHashCode() {
            var hashCode = 786948595;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Login);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CardUID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EmailAddress);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SSN);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(DateOfBirth);
            hashCode = hashCode * -1521134295 + AffiliationType.GetHashCode();
            return hashCode;
        }

        public bool Equals(Customer c) {
            if (c == null)
                return false;

            /* beware, not an elegant solution at all ahead ! DJM
             *  note : (a && b) || c <=> a && b || c
             */
            return c.FirstName.Equals(FirstName, StringComparison.OrdinalIgnoreCase)
                && c.LastName.Equals(LastName, StringComparison.OrdinalIgnoreCase)
                && c.EmailAddress.Equals(EmailAddress, StringComparison.OrdinalIgnoreCase)
                && (c.CardUID == null && CardUID == null || c.CardUID != null && CardUID != null && c.CardUID.Equals(CardUID, StringComparison.OrdinalIgnoreCase))
                && c.AffiliationType == AffiliationType
                /* these are "optional" fields, that is if they aren't set, we don't compare them ! */
                && (c.Login == null || Login == null || c.Login.Equals(Login, StringComparison.OrdinalIgnoreCase))
                && (c.SSN == null || SSN == null || c.SSN.Equals(SSN, StringComparison.OrdinalIgnoreCase))
                && (c.DateOfBirth == null || DateOfBirth == null || c.DateOfBirth == DateOfBirth);
        }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum AffiliationType {
        Student,
        Teacher,
        Staff,
        External
    }

    /// <summary>
    /// DateTiemConverter for the Json Serialization.
    /// </summary>
    public class CustomeDateTimeConverter : IsoDateTimeConverter {
        public CustomeDateTimeConverter() {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
