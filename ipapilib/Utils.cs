﻿using Newtonsoft.Json;
using System;
using System.Text;
using RestSharp;
using System.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using System.Collections.Generic;

namespace ipapilib.utils
{
    public static class Utils
    {
        public static JsonSerializerSettings jsonsettings = new JsonSerializerSettings() { ContractResolver = new EmtpyToNullContractResolver() };
        public static string ToBase64(this string plaintext)
            => Convert.ToBase64String(Encoding.UTF8.GetBytes(plaintext));

        public static bool In<T>(this T x, params T[] set)
            => set.Contains(x);

        public static string Serialize(this Customer customer)
            => JsonConvert.SerializeObject(customer, jsonsettings);

        public static T DeSerialize<T>(this string json)
            => JsonConvert.DeserializeObject<T>(json);

        public static IRestRequest AddObjBody<T>(this RestRequest restRequest, T obj)
        {
            var jsonstring = JsonConvert.SerializeObject(obj, jsonsettings);
            return restRequest.AddParameter("application/json", jsonstring, ParameterType.RequestBody);
        }

        public static TV Get<TK, TV>(this Dictionary<TK, TV> dict, TK key, TV defaultValue) {
            TV val;
            if (dict.TryGetValue(key, out val))
                return val;
            else
                return defaultValue;
        }

        public static string ToPrettyString<TK, TV>(this Dictionary<TK, TV> dict) {
            string s = "{";
            foreach (var kv in dict) {
                s += $" [{kv.Key}:{kv.Value}],";
            }
            s = s.Remove(s.Length - 1);
            s += " }";
            return s;
        }
    }



    /* https://stackoverflow.com/a/35620248/9427550 */
    public sealed class EmtpyToNullContractResolver : DefaultContractResolver {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
            if (property.PropertyType == typeof(string))
                property.ValueProvider = new EmptyToNullValueProvider(property.ValueProvider);
            return property;

        }

        sealed class EmptyToNullValueProvider : IValueProvider {
            private readonly IValueProvider valueProvider;

            public EmptyToNullValueProvider(IValueProvider valueProvider) {
                this.valueProvider = valueProvider ?? throw new ArgumentNullException("valueProvider");
            }

            public object GetValue(object target) {
                var x = (string)valueProvider.GetValue(target);
                return x == "" ? null : x;
            }

            public void SetValue(object target, object value) {
                valueProvider.SetValue(target, value);
            }
        }
    }
}
