﻿using ipapilib.utils;
using RestSharp;
using System;
using System.Net;

namespace ipapilib.Api
{
    public partial class IdentityProvisioningAPI
    {
        // TODO : rethink error handling
        public static string Rephandle(IRestResponse response)
        {
            var r = response;
            if (r.StatusCode == HttpStatusCode.Unauthorized)
                throw new UnauthorizedApiException();
            if (r.StatusCode == HttpStatusCode.InternalServerError)
                throw new Exception("An internal Error occured on the server");
            if (r.StatusDescription == "ObjectNotFound")
            {
                var o = r.Content.DeSerialize<ErrorResponse>();
                throw new ObjectNotFoundApiException($"No customer found \"{o.ObjectIdentifier}\"");
            }
            if (r.StatusDescription == "DuplicateCustomer")
                throw new DuplicateCustomerApiException(r.Content.DeSerialize<ErrorResponse>().Message);
            if (r.StatusDescription == "ParamNotValid")
                throw new ParamNotValidApiException(r.Content.DeSerialize<ErrorResponse>().Message);
            if (!r.IsSuccessful)
                throw new Exception($"{r.StatusCode.ToString()} : {r.StatusDescription}");
            
            return r.Content;
        }
    }

    public class UnauthorizedApiException : Exception
    {
        public UnauthorizedApiException() : base() {}
        public UnauthorizedApiException(string msg) : base(msg) {}
    }
    public class ObjectNotFoundApiException : Exception
    {
        public ObjectNotFoundApiException() : base() {}
        public ObjectNotFoundApiException(string msg) : base(msg) {}
    }
    public class ParamNotValidApiException : Exception
    {
        public ParamNotValidApiException() : base() {}
        public ParamNotValidApiException(string msg) : base(msg) {}
    }
    public class DuplicateCustomerApiException : Exception
    {
        public DuplicateCustomerApiException() : base() {}
        public DuplicateCustomerApiException(string msg) : base(msg) {}
    }

    public class ErrorResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string ObjectType { get; set; }
        public string ObjectIdentifier { get; set; }
    }
}