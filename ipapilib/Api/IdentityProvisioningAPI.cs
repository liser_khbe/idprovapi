﻿using ipapilib.utils;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ipapilib.Api
{
    // TODO : refactor 
    public partial class IdentityProvisioningAPI : IDAL
    {
        public APIConfig Config { get; private set; }

        /// <summary>
        /// Get a customer by its login. [GET /v1/customer/login/{login}]
        /// </summary>
        public Customer GetCustomerByLogin(string login) {
            var response = Config.Client.Execute(getCustomerByLoginRequest(login));
            return Rephandle(response).DeSerialize<Customer>();
        }
        /// <summary>
        /// Get a customer by its login asynchronously and execute callback with response. 
        /// [GET /v1/customer/login/{login}]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void GetCustomerByLoginAsync<T>(string login, Action<T> callback) {
            Config.Client.ExecuteAsync(
                getCustomerByLoginRequest(login),
                callback as Action<IRestResponse>);
        }
        private IRestRequest getCustomerByLoginRequest(string login) {
            var r = new RestRequest(Config.Prefix+"customer/login/{login}", Method.GET);
            r.AddUrlSegment("login", login);
            return r;
        }

        /// <summary>
        /// Get a customer by its UID. [GET /v1/customer/uid/{uid}]
        /// </summary>
        public Customer GetCustomerByUID(string uid) {
            var response = Config.Client.Execute(getCustomerByUIDRequest(uid));
            return Rephandle(response).DeSerialize<Customer>();
        }
        /// <summary>
        /// Get a customer by its UID asynchronously and execute callback with response. 
        /// [GET /v1/customer/uid/{uid}]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void GetCustomerByUIDAsync<T>(string uid, Action<T> callback) {
            Config.Client.ExecuteAsync(
                getCustomerByUIDRequest(uid),
                callback as Action<IRestResponse>);
        }
        private IRestRequest getCustomerByUIDRequest(string uid) {
            var r = new RestRequest(Config.Prefix + "customer/uid/{uid}", Method.GET);
            r.AddUrlSegment("uid", uid);
            return r;
        }

        /// <summary>
        /// Create a customer based on Customer object.
        /// [POST /v1/customer]
        /// </summary>
        public string CreateCustomer(Customer customer) {
            var response = Config.Client.Execute(createCustomerRequest(customer));
            return Rephandle(response);
        }
        /// <summary>
        /// Create a customer based on Customer object Asynchronously.
        /// [POST /v1/customer]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void CreateCustomerAsync<T>(Customer customer, Action<T> callback) {
            Config.Client.ExecuteAsync(
                createCustomerRequest(customer),
                callback as Action<IRestResponse>);
        }
        private IRestRequest createCustomerRequest(Customer customer) {
            var r = new RestRequest(Config.Prefix + "customer", Method.POST);
            r.AddObjBody(customer);
            return r;
        }

        /// <summary>
        /// Update a customer based on Customer object.
        /// [PUT /v1/customer]
        /// </summary>
        public void UpdateCustomer(Customer customer) {
            var response = Config.Client.Execute(updateCustomerRequest(customer));
            Rephandle(response);
        }
        /// <summary>
        /// Update a customer based on Customer object Asynchronously.
        /// [PUT /v1/customer]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void UpdateCustomerAsync<T>(Customer customer, Action<T> callback) {
            Config.Client.ExecuteAsync(
                updateCustomerRequest(customer),
                callback as Action<IRestResponse>);
        }
        private IRestRequest updateCustomerRequest(Customer customer) {
            var r = new RestRequest(Config.Prefix + "customer", Method.PUT);
            r.AddObjBody(customer);
            return r;
        }

        /// <summary>
        /// Set/Update de UID of a customer identified by its login.
        /// [PATCH /v1/customer/{login}/{uid}]
        /// </summary>
        public void SetCustomerUID(string login, string uid) {
            var response = Config.Client.Execute(setCustomerUIDRequest(login, uid));
            Rephandle(response);
        }
        /// <summary>
        /// Set/Update de UID of a customer identified by its login Asynchronously.
        /// [PATCH /v1/customer/{login}/{uid}]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void SetCustomerUIDAsync<T>(string login, string uid, Action<T> callback) {
            Config.Client.ExecuteAsync(
                setCustomerUIDRequest(login, uid),
                callback as Action<IRestResponse>);
        }
        private IRestRequest setCustomerUIDRequest(string login, string uid) {
            var r = new RestRequest(Config.Prefix + "customer/{login}/{uid}", Method.PATCH);
            r.AddUrlSegment("login", login);
            r.AddUrlSegment("uid", uid);
            return r;
        }

        /// <summary>
        /// Delete a customer identified by its login.
        /// [DELETE /v1/customer/{login}]
        /// </summary>
        public void DeleteCustomer(string login) {
            var response = Config.Client.Execute(deleteCustomerRequest(login));
            Rephandle(response);
        }
        /// <summary>
        /// Delete a customer identified by its login Asynchronously.
        /// [DELETE /v1/customer/{login}]
        /// </summary>
        /// <typeparam name="T">IRestResponse</typeparam>
        /// <param name="callback">Action with IRestResponse</param>
        public void DeleteCustomerAsync<T>(string login, Action<T> callback) {
            Config.Client.ExecuteAsync(
                deleteCustomerRequest(login),
                callback as Action<IRestResponse>);
        }
        private IRestRequest deleteCustomerRequest(string login) {
            var r = new RestRequest(Config.Prefix + "customer/{login}", Method.DELETE);
            r.AddUrlSegment("login", login);
            return r;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public IdentityProvisioningAPI(APIConfig configuration) {
            Config = configuration;
        }
    }
}
