﻿using RestSharp;
using RestSharp.Authenticators;
using System.Collections.Generic;

namespace ipapilib.Api
{
    public static class Defaults
    {
        /* HardCoded Defaults Defaults */
        public const string version = "v1";
        #if DEBUG
        public const string host = "https://ssldev.education.lu";
        #else
        public const string host = "https://ssl.education.lu";
        #endif
        public const string basepath = "/eRestauration/API/IdentityProvisioning";
        public static readonly Dictionary<string,string> defaultHeader = new Dictionary<string, string> {
            { "Content-Type", "application/json" },
            { "Accept", "application/json" }
        };
    }
    public class APIConfig
    {
        public string Host { get; private set; }
        public string BasePath { get; private set; }
        public string Prefix { get; private set; }
        public RestClient Client { get; private set; }
        public string Username { get; private set; }
        public Dictionary<string,string> DefaultHeader { get; private set; }

        public APIConfig(string username, string passphrase,
            string host=Defaults.host, string basepath=Defaults.basepath, 
            string prefix=Defaults.version, Dictionary<string, string> defaultHeader=null)
        {
            Host = host;
            BasePath = basepath;
            Username = username;
            Prefix = prefix+"/";
            DefaultHeader = defaultHeader?? Defaults.defaultHeader;
            
            Client = new RestClient(Host + BasePath);
            Client.Authenticator = new HttpBasicAuthenticator(username, passphrase);
            foreach (var kv in DefaultHeader)
                Client.AddDefaultHeader(kv.Key, kv.Value);
        }

    }
}
