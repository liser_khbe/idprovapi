﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ipapilib.Api;
using ipapilib.utils;

namespace IdProvClient.IO {
    public class SaveConf {
        public const string slogin = "login";
        public const string spassw = "pass";
        public const string shost  = "host";

        public const string defaultfilename = "idprov_conf.bin";
        public string Filename { get; set; }

        public SaveConf(string filename = defaultfilename) {
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            Filename = $"{dir}\\{filename}";
        }
        /// <summary>
        /// Save the dict in a file and encrypt it.
        /// </summary>
        public void Save(Dictionary<string, string> conf) {
            var a = conf.Select(x => $"{x.Key}:{x.Value}\n").Aggregate("", string.Concat);
            EncryptInFile(a, Filename);
        }
        /// <summary>
        /// Returns the content of the conf file as a Dictionnary ("key:pair\n")
        /// </summary>
        public Dictionary<string, string> Load() {
            try {
                return Convert(DecryptFile(Filename).ToArray());
            } catch (FileNotFoundException e) {
                throw e;
            } catch (Exception) {
                return new Dictionary<string, string>();
            }
        }

        /// <summary>
        /// Returns an array of all the Options.
        /// </summary>
        /// <param name="keys">List of the keys of options to get.</param>
        public string[] GetOpts(string []keys) {
            /* Refactor it to simply use the Dict cause might as well just use the dict.
             * This actually used to return a Triple, that's why I'm using an array now.
             */
            Dictionary<string, string> opt;
            try {
                opt = Load();
            } catch (FileNotFoundException) {
                Console.WriteLine("No config found.");
                opt = new Dictionary<string, string>();
            }
            List<string> vs = new List<string>();
            foreach(var k in keys) {
                vs.Add(opt.Get(k, ""));
            }
            return vs.ToArray();
        }
        /// <summary>
        /// Create an instance of APIConfig with a configuration Dictionnary that could be returned from Load().
        /// </summary>
        /// <param name="conf"></param>
        public static APIConfig CreateConf(Dictionary<string, string> conf) { 
            if (conf.ContainsKey(shost))
                return new APIConfig(conf.Get(slogin, ""), conf.Get(spassw,""), conf[shost]);
            return new APIConfig(conf.Get(slogin, ""), conf.Get(spassw,""));
        }

        /// <summary>
        /// Converts an array of string in a Dictionary. ("{key}:{pair}")
        /// </summary>
        public static Dictionary<string, string> Convert(string[] lines) {
            var opts = new Dictionary<string, string>();
            foreach (var l in lines) {
                var (a, b) = splitline(l);
                if (a != "" && b != "") {
                    try {
                        opts.Add(a, b);
                    } catch {}
                }
            }
            return opts;

            (string, string) splitline(string l) {
                var i = l.IndexOf(':');
                if (i == -1)
                    return ("", "");
                var a = l.Substring(0, i).Trim();
                var b = l.Substring(i + 1).Trim();
                return (a, b);
            }
        }
        /// <summary>
        /// Writes a string to a file after encrypting it.
        /// </summary>
        /// <param name="plaintxt">String to be encrypted and saved.</param>
        /// <param name="filename">Name of the file to save in.</param>
        private static void EncryptInFile(string plaintxt, string filename) {
            var RMCrypto = new RijndaelManaged();
            using (var file = new FileStream(filename, FileMode.Create))
            using (var cs = new CryptoStream(file, RMCrypto.CreateEncryptor(key, iv), CryptoStreamMode.Write))
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(plaintxt))) {
                int data;
                while ((data = stream.ReadByte()) != -1)
                    cs.WriteByte((byte)data);
            }
        }
        /// <summary>
        /// Get the lines of plaintext of an encrypted file as an IEnumerable.
        /// </summary>
        /// <param name="filename">Name of the file to load from.</param>
        private static IEnumerable<string> DecryptFile(string filename) {
            var RMCrypto = new RijndaelManaged();
            using (FileStream file = new FileStream(filename, FileMode.Open))
            using (CryptoStream cs = new CryptoStream(file, RMCrypto.CreateDecryptor(key, iv), CryptoStreamMode.Read))
            using (StreamReader r = new StreamReader(cs)) {
                string line;
                while ((line = r.ReadLine()) != null)
                    yield return line;
            }
        }


        private static readonly byte[] key = Encoding.UTF8.GetBytes("thisisaverlongpasswordthatnobodyshouldevenbothertotryfinding".Substring(0, 128 / 8));
        private static readonly byte[] iv = Encoding.UTF8.GetBytes("Idontevenknowwhyimbotheringhavingadifferentivbecausethisisntsecureanyway".Substring(0, 128 / 8));
    }
}
