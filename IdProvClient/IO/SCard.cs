﻿using PCSC;
using PCSC.Iso7816;
using PCSC.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdProvClient.IO {
    public class SCard {
        /// <summary>
        /// Collection of card monitors. Key is their name.
        /// In OnStatusChanged we can only get the name of the reader, this is a way to get 
        /// an Instance of the Card monitor with that name.
        /// </summary>
        private Dictionary<string, ISCardMonitor> monitors;
        /// <summary>
        /// Device monitor
        /// </summary>
        private IDeviceMonitor dmon;
        /// <summary>
        /// Action to execute after the a card has been inserted.
        /// Takes the UID of the inserted card as parameter
        /// </summary>
        public Action<string> UidCallback{ get; set; }
        /// <summary>
        /// Whether or not the CardReader passes the UID In BigEndian or not.
        /// </summary>
        public bool BigEndian { get; set; }

        /// <summary>
        /// Initialize properties, start device monitor and subscribe to DeviceMonitor's event
        /// for the hotpluggableness.
        /// </summary>
        /// <param name="uidcallback"></param>
        /// <param name="bigendian"></param>
        public SCard(Action<string> uidcallback, bool bigendian=false) {
            UidCallback = uidcallback;
            BigEndian = bigendian;
            monitors = new Dictionary<string, ISCardMonitor>();
            // StartMonitoring();
            var dfactory = DeviceMonitorFactory.Instance;
            dmon = dfactory.Create(SCardScope.System);
            dmon.Initialized += OnInitialized;
            dmon.StatusChanged += OnStatusChanged;
            dmon.MonitorException += OnMonitorException;

            dmon.Start();
        }
        /// <summary>
        /// Unsubscribe to all events, Dispose() all ressources such as monitors, device monitor.
        /// </summary>
        ~SCard() {
            dmon.Initialized -= OnInitialized;
            dmon.StatusChanged -= OnStatusChanged;
            dmon.MonitorException -= OnMonitorException;
            try {
                dmon.Cancel();
                dmon.Dispose();
            } catch {}
            try {
                foreach (var m in monitors.Values) {
                    m.Cancel();
                    m.Dispose();
                }
            } finally {
                monitors.Clear();
            } 
        }

        /// <summary>
        /// Unsubscribe all the events and Dispose() the monitor.
        /// </summary>
        /// <param name="rdr">Name of the CardReader</param>
        public void RemoveCardMonitor(string rdr) {
            // TODO : comments (needed ?)
            ISCardMonitor m;
            if (!monitors.TryGetValue(rdr, out m)) return;
            m.CardInserted -= OnCardInsert;
            m.CardRemoved -= OnCardRemove;
            try {
                m.Cancel();
                m.Dispose();
            } finally {
                monitors.Remove(rdr);
            }
        }
        /// <summary>
        /// Subscribes the reader to the OnCardInsert and OnCardRemove.
        /// Also adds the monitor of the reader to the monitor list for disposing it.
        /// </summary>
        /// <param name="rdr">Name of the CardReader</param>
        private void AddCardMonitor(string rdr) {
            // TODO : comments (needed ?)
            var cmon = MonitorFactory.Instance.Create(SCardScope.System);
            var k = rdr;
            cmon.CardInserted += OnCardInsert;
            cmon.CardRemoved += OnCardRemove;
            if(monitors.ContainsKey(rdr)) {
                try {
                    monitors[rdr].Cancel();
                    monitors[rdr].Dispose();
                } finally {
                    monitors.Remove(rdr);
                }
            }
            monitors.Add(k, cmon);
            cmon.Start(rdr);
        }
        /// <summary>
        /// Do nothing (yet?)
        /// </summary>
        private void OnCardRemove(object sender, CardStatusEventArgs e) {
        }
        /// <summary>
        /// OnCardInsertion, query the card's uid, and pass it to the UidCallback Action.
        /// </summary>
        private void OnCardInsert(object sender, CardStatusEventArgs e) {
            try {
                var a = GetUID(e.ReaderName);
                UidCallback?.Invoke(a);
            } catch {}
        }
        /// <summary>
        /// Do nothing.
        /// </summary>
        private void OnMonitorException(object sender, DeviceMonitorExceptionEventArgs args) {
            /* Never had this even triggered.
             * Except once on a machine without the drivers.
             */
            Console.WriteLine("OnMonitorException");
        }
        /// <summary>
        /// When a CardReader is plugged or unplugged.
        /// Add or Remove it from collection and subscribe or unsubscribe to all the 
        /// Card-related events (insertion, removal).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStatusChanged(object sender, DeviceChangeEventArgs e) {
            /* Basically the readers are a detached entity of the device itself.
             * Our Device has 2 readers in the firmware, but only one that is usable...
             * To avoid dirty and confusing hacks to catch the *right* reader, we just add them all.
             * 
             * When a reader is Attached, we also subscribe to the Card insertion and card removal events 
             * on that reader.
             */
            foreach(var m in e.DetachedReaders) {
                RemoveCardMonitor(m);
            }
            foreach(var m in e.AttachedReaders) {
                AddCardMonitor(m);
            }
        }
        /// <summary>
        /// On initialization of the Device, add an event to each reader (to monitor the insertion of a card).
        /// </summary>
        private void OnInitialized(object sender, DeviceChangeEventArgs e) {
            /* the reason to subscribe every reader in an envent is twofolds :
             * 1. makes it hotpluggable
             * 2. There are multiple readers per device, it is tedious to know which one we need, and 
                  easier to just monitor all of them. 
             */
            foreach(var m in e.AllReaders) {
                AddCardMonitor(m);
            }
        }

        /// <summary>
        /// Query the specified reader to read the UID of the card. Can throw exception if no reader is found.
        /// </summary>
        /// <param name="rdr">Name of the cardreader</param>
        private string GetUID(string rdr) {
            var ctxF = ContextFactory.Instance;
            using (var ctx = ctxF.Establish(SCardScope.System))
            using (var rfidr = ctx.ConnectReader(rdr, SCardShareMode.Shared, SCardProtocol.Any)) {
                /* create the payload */
                var apdu = new CommandApdu(IsoCase.Case2Short, rfidr.Protocol) {
                    CLA = 0xFF,
                    INS = 0xCA,
                    P1 = 0x00,
                    P2 = 0x00,
                    Le = 0
                };

                using (rfidr.Transaction(SCardReaderDisposition.Leave)) {
                    var sendPci = SCardPCI.GetPci(rfidr.Protocol);
                    var receivePci = new SCardPCI();

                    var receiveBuffer = new byte[32];
                    byte[] command = apdu.ToArray();

                    // transmit our payload.
                    int received = rfidr.Transmit(
                        sendPci,
                        command,
                        command.Length,
                        receivePci,
                        receiveBuffer, //this is like passing a pointer for the response in C.
                        receiveBuffer.Length);

                    // Construct a REsponseApdu object with our response (in receiveBuffer)
                    var responseApdu =
                        new ResponseApdu(receiveBuffer, received, IsoCase.Case2Short, rfidr.Protocol);

                    if (!responseApdu.HasData)
                        return "";
                    return BitConverter.ToString(BigEndian 
                            ? responseApdu.GetData() 
                            : responseApdu.GetData().Reverse().ToArray())
                        .Replace("-", string.Empty);
                }
            }
        }
    }
}
