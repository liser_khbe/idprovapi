﻿using IdProvClient.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using ipapilib.utils;
using ipapilib.Api;

namespace IdProvClient {
    /// <summary>
    /// Interaction logic for Options.xaml
    /// </summary>
    public partial class OptionsWindow : Window {
        private readonly string vlogin;
        private readonly string vpassw;
        private readonly string vhost;

        public const string filename = "api.bin";

        public OptionsWindow() {
            var conf = new SaveConf(filename);
            var l = conf.GetOpts(new string[] { SaveConf.slogin, SaveConf.spassw, SaveConf.shost});
            vlogin = l[0];
            vpassw = l[1];
            vhost  = l[2];

            Loaded += (s, e) => {
                (loginbox.Text, passbox.Password, hostbox.Text) = (vlogin, vpassw, vhost);
                hostbox.Tag = ipapilib.Api.Defaults.host;

            };
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            var input = new Dictionary<string, string> {
                {SaveConf.slogin, loginbox.Text == string.Empty ? vlogin : loginbox.Text},
                {SaveConf.spassw, passbox.Password == string.Empty ? vpassw : passbox.Password},
            };
            string h;
            if ((h = hostbox.Text == string.Empty ? vhost : hostbox.Text) != string.Empty) {
                input.Add(SaveConf.shost, h);
            }

            Mvvm.Api = new IdentityProvisioningAPI(SaveConf.CreateConf(input));

            var s = new SaveConf(filename);
            try {
                s.Save(input);
            } catch {
                // TODO : Saving failed
            }
            this.Close();
        }
    }
}
