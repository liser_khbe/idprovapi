﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace IdProvClient.Converters {

    class StringToBool : IMultiValueConverter {
        /// <summary>
        /// Converts an array of string to a bool, testing if they're nullOrEmpty. Default is ANDing.
        /// </summary>
        /// <param name="parameter">
        /// string containing "and" | "or" (default and), operation on results of predicate. 
        /// "negate" : negates end result.
        /// </param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            var p = ((string)parameter??string.Empty).ToLower();
            (bool op, bool negate) = ParseParams(p);
            bool val;
            /* if op is 'and' (default)*/
            if (op)
                /* only true if _none_ are empty */ 
                val = !values.Cast<string>().Any(String.IsNullOrEmpty);
            else
                /* only false if they're _All_ empty */
                val = !values.Cast<string>().All(String.IsNullOrEmpty);

            return negate ? !val : val;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
            Console.WriteLine("NotImplementedException");
            return new object[] { };
        }
        private static (bool, bool) ParseParams(string parameter) 
            // default 'and'
            => (!parameter.Contains("or") ,parameter.Contains("negate"));
        
    }
} 