﻿using System;
using System.Globalization;
using System.Windows.Data;


namespace IdProvClient.Converters {
    static class VisibilityConverter {
        public static object FromBool(bool value, bool hidden = true, bool negate = false) {
            bool val = negate ? !value : value;
            return val ? "Visible" : hidden ? "Hidden" : "Collapsed";
        }
        /// <summary>
        /// Returns a tuple dependant on the parameter containing hidden and negate.
        /// </summary>
        /// <param name="parameter">string containing collapse or hidden (default hidden)</param>
        public static (bool, bool) ParseParams(string parameter)
            => (!parameter.Contains("collapse"), parameter.Contains("negate"));
        
    }
    class BoolToVisibilityConverter : IValueConverter {
        /// <summary>
        /// Converts a boolean to a Visibility value.
        /// </summary>
        /// <param name="parameter">"collapse | hidden" : how to handle false (default hidden)\n
        ///                         "negate" : visible when false, and vice-versa</param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            string ps = ((string)parameter).ToLower();
            (bool hidden, bool negate) = VisibilityConverter.ParseParams(ps);
            return VisibilityConverter.FromBool((bool)value, hidden, negate);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    class StringToVisibilityConverter : IValueConverter {
        /// <summary>
        /// Convers a string to a Visibility value depending on whether it is null/empty or not.
        /// </summary>
        /// <param name="parameter">"collapse | hidden" : how to handle false (default hidden)\n
        ///                         "negate" : visible when false, and vice-versa</param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var s = (string)value;
            string ps = ((string)parameter).ToLower();
            (bool hidden, bool negate) = VisibilityConverter.ParseParams(ps);
            return VisibilityConverter.FromBool(String.IsNullOrEmpty(s), hidden, negate);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
