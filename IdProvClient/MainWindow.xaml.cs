﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IdProvClient {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public Mvvm mvvm { get; set; }
        private const string searchstr = "Search customer by ";
        public MainWindow() {
            Loaded += OnLoad_setDefaults;
            mvvm = new Mvvm();
            InitializeComponent();
        }
        /// <summary>
        /// Default values, events, DataContext to set when window is finished loading.
        /// </summary>
        private void OnLoad_setDefaults(object sender, RoutedEventArgs e) {
            search_type.SelectionChanged += Search_type_SelectionChanged;
            search_bar.TextChanged += Search_bar_TextChanged;
            if (affiliationbox.HasItems)
                affiliationbox.SelectedIndex = 0;

            DataContext = this;

            try {
                checkPrivilege();
            } catch (Exception) {
                Kill("Unexpected Error while verifying your access rights. Contact your administrator.");
            }
            loadConfig();
        }

        #region startup_verification
        /// <summary>
        /// Load Configuration and make into Mvvm.Api.
        /// </summary>
        private void loadConfig() {
            var s = new IO.SaveConf(OptionsWindow.filename);
            Dictionary<string, string> opts;
            try {
                opts = s.Load();
            } catch { 
                 opts = new Dictionary<string, string>();
            } 
            Mvvm.Api = new ipapilib.Api.IdentityProvisioningAPI(IO.SaveConf.CreateConf(opts));
        }

        private const string authorized_group = "CN=restopolis,OU=_Groups,OU=Belval,DC=liser,DC=local";
        /// <summary>
        /// Check whether user is part of the right AD group.
        /// If not, or on error, kill().
        /// </summary>
        private void checkPrivilege() {
            DirectorySearcher srch = new DirectorySearcher();
            srch.SearchScope = SearchScope.Subtree;
            srch.Filter = $"(sAMAccountName={Environment.UserName})";
            SearchResult res = srch.FindOne();

            if (!res.Properties.Contains("memberof"))
                Kill("Could not Find your group. Please contact your administrator.");
            if (!res.Properties["memberOf"].Contains(authorized_group))
                Kill("Unauthorized access! Contact your administrator.");
        }
        #endregion

        /// <summary>
        /// Show MessageBox with message and exit application.
        /// </summary>
        private void Kill(string msg) {
            MessageBox.Show(msg);
            Environment.Exit(-1);
        }

        // TODO : only allow edit/set-carduid/delete when Login (and uid) is set
        private void New_Click(object sender, RoutedEventArgs e) {
            mvvm.ModifyAll();
            mvvm.Reset();
            commit_btn.Tag = "create";
        }
        private void Mod_Click(object sender, RoutedEventArgs e) {
            mvvm.ModifyAll();
            commit_btn.Tag = "update";
        }
        private void Delete_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show(
                "Are you sure you want to delete this customer ?", 
                "Delete confirmation",
                MessageBoxButton.YesNo);
            if(result == MessageBoxResult.Yes)
                Mvvm.Api.DeleteCustomerAsync(mvvm.CurrentCustomer.Login, mvvm.sendgetCallback);
        }
        // [DJM] very dirty way of doing this... I know, I'm lacking inspiration right now...
        private void AddCard_Click(object sender, RoutedEventArgs e) {
            mvvm.ModifyUid();
            commit_btn.Tag = "setcard";
        }
        private void Cancel_Click(object sender, RoutedEventArgs e) {
            mvvm.CancelAll();
            mvvm.Undo();
            commit_btn.Tag = "";
        }
        private void Clear_Click(object sender, RoutedEventArgs e) {
            mvvm.Clear();
        }
        // [DJM] very dirty way of doing this... I know, I'm lacking inspiration right now...
        /// <summary>
        /// Check the tag on the commit button (set by the Modify, New, Add Carduid) to determine
        /// what to do .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Commit_Click(object sender, RoutedEventArgs e) {
            // TODO : don't allow the click if fields are not legal (uid length, email regex, etc.)
            var tb = (Button)sender;
            var op = (string)tb.Tag;
            if (op == "setcard" && mvvm.CurrentCustomer.CardUID != string.Empty) {
                Mvvm.Api.SetCustomerUIDAsync(
                    mvvm.CurrentCustomer.Login,
                    mvvm.CurrentCustomer.sanitizedCardUID(),
                    mvvm.sendgetCallback);
            } else if (op == "update" || op == "setcard") {
                mvvm.CurrentCustomer.CardUID = mvvm.CurrentCustomer.sanitizedCardUID();
                Mvvm.Api.UpdateCustomerAsync(mvvm.CurrentCustomer, mvvm.sendgetCallback);
            } else {
                Mvvm.Api.CreateCustomerAsync(mvvm.CurrentCustomer, mvvm.sendgetCallback);
            }
            mvvm.CancelAll();
            tb.Tag = "";
        }
        /// <summary>
        /// Open Credentials dialog.
        /// </summary>
        private void Cred_Click(object sender, RoutedEventArgs e) {
            var win = new OptionsWindow();
            win.Owner = this;
            win.ShowDialog();
            win.Hide();
        }

        private void Search_bar_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Escape)
                Console.WriteLine("esc");
            if (e.Key != Key.Return)
                return;

            string content = ((TextBox)sender).Text;
            int i;
            if ((i = content.IndexOf(':')) != -1)
                content = content.Substring(++i);

            if (search_type.SelectedIndex == 1)
                Mvvm.Api.GetCustomerByUIDAsync(content, mvvm.getCallback);
            else
                Mvvm.Api.GetCustomerByLoginAsync(content, mvvm.getCallback);
        }
        /// <summary>
        /// Change placeholder ("Search Customer by {Login, UID}") text when Searchtype changes.
        /// </summary>
        private void Search_type_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var item = (ComboBoxItem)((ComboBox)sender).SelectedItem;
            var s = searchstr + item.Content.ToString().ToLower();
            search_bar.Tag = s;
        }
        /// <summary>
        /// Sets Login/UID type of search by typing "login:" or "uid:"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_bar_TextChanged(object sender, TextChangedEventArgs e) {
            var box = (TextBox)sender;
            var content = box.Text;
            if (!content.Contains(':'))
                return;
            if (content.ToLower().StartsWith("login:")) {
                search_type.SelectedIndex = 0;
                box.Text = content.Substring("login:".Length);
            }
            if (content.ToLower().StartsWith("uid:")) {
                search_type.SelectedIndex = 1;
                box.Text = content.Substring("uid:".Length);
            }
        }
    }
}