﻿using System;
using System.ComponentModel;
using ipapilib;

namespace IdProvClient.Model {
    /// <summary>
    /// subclass to implement the INotifyPropertyChanged on every Field.
    /// </summary>
    public class Customer : ipapilib.Customer, INotifyPropertyChanged {
        public static Customer Empty()
            => new Customer {
                Login = "",
                CardUID = "",
                FirstName = "",
                LastName = "",
                EmailAddress = "",
                SSN = "",
                DateOfBirth = DateTime.Now,
                AffiliationType = AffiliationType.Student
            };

        /// <summary>
        /// Json Converter requires default constructor.
        /// </summary>
        public Customer() {}
        /// <summary>
        /// Construct instance from Customer of the ipapilib.
        /// i.e. it's the same but with the INotifyPropertyCahnged.
        /// </summary>
        /// <param name="c"></param>
        public Customer(ipapilib.Customer c) {
            Login = c.Login;
            CardUID = c.CardUID;
            FirstName = c.FirstName;
            LastName = c.LastName;
            EmailAddress = c.EmailAddress;
            SSN = c.SSN;
            DateOfBirth = c.DateOfBirth;
            AffiliationType = c.AffiliationType;
        }

        public override string Login {
            get => base.Login;
            set { base.Login = value; OnPropertyChanged("Login"); }
        }
        public override string CardUID {
            get => base.CardUID;
            set { base.CardUID = value; OnPropertyChanged("CardUID"); }
        }
        public override string FirstName {
            get => base.FirstName;
            set { base.FirstName = value; OnPropertyChanged("FirstName"); }
        }
        public override string LastName {
            get => base.LastName;
            set { base.LastName = value; OnPropertyChanged("LastName"); }
        }
        public override string EmailAddress {
            get => base.EmailAddress;
            set { base.EmailAddress = value; OnPropertyChanged("EmailAddress"); }
        }
        public override string SSN {
            get => base.SSN;
            set { base.SSN = value; OnPropertyChanged("SSN"); }
        }
        public override DateTime? DateOfBirth {
            get => base.DateOfBirth;
            set { base.DateOfBirth = value; OnPropertyChanged("DateOfBirth"); }
        }
        public override AffiliationType AffiliationType {
            get => base.AffiliationType;
            set { base.AffiliationType = value; OnPropertyChanged("AffiliationType"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name) {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// If user enterred uid separated by dashes, remove all the dashes.
        /// </summary>
        /// <returns></returns>
        public string sanitizedCardUID() {
            string s = CardUID;
            return s.Replace("-", "");
        }
    }
}