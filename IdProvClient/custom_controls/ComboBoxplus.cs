﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace IdProvClient.custom_controls {
    /// <summary>
    /// Combobox with a minWidth matching its longetst element
    /// credits : http://blogs.microsoft.co.il/pavely/2010/08/31/wpf-auto-size-combobox/
    /// </summary>
    class ComboBoxplus : ComboBox {
        private int _selected;
        public override void OnApplyTemplate() {
            base.OnApplyTemplate();
            _selected = SelectedIndex;
            SelectedIndex = -1;
            Loaded += OnLoad_ComboBoxplus;
        }

        private void OnLoad_ComboBoxplus(object sender, RoutedEventArgs e) {
            var popup = GetTemplateChild("PART_Popup") as Popup;
            var content = popup.Child as FrameworkElement;
            content.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            MinWidth = content.DesiredSize.Width + ActualHeight;
            SelectedIndex = this.HasItems ? 0 : _selected;
        }
    }
}