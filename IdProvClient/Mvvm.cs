﻿using System;
using System.Threading.Tasks;
using IdProvClient.Model;
using IdProvClient.IO;
using RestSharp;
using ipapilib.utils;
using System.Net;
using System.ComponentModel;

namespace IdProvClient {
    public class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
    public class Mvvm : Notifier {
        #region private_fields
        private bool edit_all;
        private bool edit_uid;
        private Customer ccustomer;
        private Customer pcustomer;
        private string status;
        private SCard card;
        #endregion

        #region properties
        public static ipapilib.IDAL Api { get; set; }
        public bool EditAll {
            get => edit_all;
            set {
                edit_all = value;
                OnPropertyChanged("EditAll");
            }
        }
        public bool EditUid {
            get => edit_uid;
            set {
                edit_uid = value;
                OnPropertyChanged("EditUid");
            }
        }
        public Customer CurrentCustomer {
            get => ccustomer;
            set {
                ccustomer = value;
                OnPropertyChanged("CurrentCustomer");
            }
        }
        public string Status {
            get => status;
            set {
                status = value;
                OnPropertyChanged("Status");
            }
        }
        #endregion

        public void ModifyAll() {
            EditAll = true;
            EditUid = true;
            pcustomer = CurrentCustomer;
        }
        public void ModifyUid() {
            pcustomer = CurrentCustomer;
            EditUid = true;
        }
        public void CancelAll() {
            EditAll = false;
            EditUid = false;
        }
        public void Reset() {
            CurrentCustomer = Customer.Empty();
        }
        /// <summary>
        /// Same as Reset() but keep the Login name
        /// </summary>
        public void Clear() {
            if (edit_all) {
                var u = CurrentCustomer.Login;
                CurrentCustomer = Customer.Empty();
                CurrentCustomer.Login = u;
            } else if (edit_uid) {
                CurrentCustomer.CardUID = string.Empty;
            }
        }
        public void Undo() {
            CurrentCustomer = pcustomer;
        }

        /// <summary>
        /// constructor
        /// </summary>
        public Mvvm() {
            EditAll = false;
            EditUid = false;
            CurrentCustomer = Customer.Empty();
            pcustomer = Customer.Empty();
            initCallbacks();
            card = new SCard(uid => {
                Status = "Card detected.";
                Api.GetCustomerByUIDAsync(uid, (IRestResponse x) => cardCallback(x, uid));
            });
        }

        /// <summary>
        /// Resets the status propriety after i milliseconds
        /// </summary>
        /// <param name="i">Delay in milliseconds</param>
        private async void ResetStatusAsync(int i) {
            await Task.Delay(i);
            Status = "";
        }

        /// <summary>
        /// Write in the Statusbar. For the moment it's a rather ugly thing : a 2Way data-binding on a string
        /// </summary>
        /// <param name="description">HttpDescription</param>
        /// <param name="content">Content/body</param>
        /// <returns></returns>
        private string httpStatusString(HttpStatusCode code, string description, string content) {
            string preambule = $"[{code} {description}] ";
            string precodeless = $"[{description}] ";
            switch (code) {
                case HttpStatusCode.OK:
                    return "OK";
                case HttpStatusCode.InternalServerError:
                    return preambule + "An Error Occurred on the server. " +
                            "Try again or contact your administrator";
                case HttpStatusCode.NotFound:
                    return preambule + "Not Found.";
                case HttpStatusCode.Unauthorized:
                    return "Unauthorized. Please verify your username and password.";
                case HttpStatusCode.BadRequest:
                    return badrequeststring();
                default:
                    Console.WriteLine($"[{code}, {description}] --- unexpected error");
                    return preambule + "Unexpected Error. Try again or contact your administrator.";
            }
            string badrequeststring() {
                if (description == "ObjectNotFound") {
                    return $"No customer found with \"{content.DeSerialize<ipapilib.Api.ErrorResponse>().ObjectIdentifier}\"";
                } else if (description.In(new string[] { "DuplicatCustomer", "ParamNotValid", "Customerlocked" })) {
                    return precodeless + content.DeSerialize<ipapilib.Api.ErrorResponse>().Message;
                } else {
                    return preambule + content;
                }
            }
        }

        #region callbacks
        /// <summary>
        /// Fill the view (CurrentCustomer) with the response
        /// </summary>
        public Action<IRestResponse> getCallback;
        /// <summary>
        /// Only handle Response as Statuscode
        /// </summary>
        public Action<IRestResponse> sendCallback;
        /// <summary>
        /// After response, do a get request (+ getCallback) with login.
        /// </summary>
        public Action<IRestResponse> sendgetCallback;
        /// <summary>
        /// Special callback for a GetUID() on event of inserted card.
        /// </summary>
        public Action<IRestResponse, string> cardCallback;
        #endregion
        /// <summary>
        /// Initialize the previous callbacks.
        /// </summary>
        private void initCallbacks() {
            getCallback = delegate (IRestResponse r) {
                if (r.IsSuccessful)
                    CurrentCustomer = new Customer(r.Content.DeSerialize<ipapilib.Customer>());
                else
                    Reset();
                Status = httpStatusString(r.StatusCode, r.StatusDescription, r.Content);
                ResetStatusAsync(Status == "OK" ? 1000 : 8000);
            };
            sendgetCallback = delegate (IRestResponse r) {
                if (r.IsSuccessful && r.Content != "") {
                    Api.GetCustomerByLoginAsync(r.Content.Trim('"'), getCallback);
                } else if (r.IsSuccessful && CurrentCustomer.Login != "") {
                    Api.GetCustomerByLoginAsync(CurrentCustomer.Login, getCallback);
                } else {
                    Status = httpStatusString(r.StatusCode, r.StatusDescription, r.Content);
                    ResetStatusAsync(8000);
                };
            };
            sendCallback = delegate (IRestResponse r) {
                Status = httpStatusString(r.StatusCode, r.StatusDescription, r.Content);
                ResetStatusAsync(Status == "OK" ? 1000 : 8000);
            };
            cardCallback = delegate (IRestResponse r, string uid) {
                var s = httpStatusString(r.StatusCode, r.StatusDescription, r.Content);
                var t = true;
                if (r.IsSuccessful && (edit_all || edit_uid)) {
                    Status = "This card is already owned by another customer";
                    t = false;
                } else if (edit_all || edit_uid) {
                    CurrentCustomer.CardUID = uid;
                    t = true;
                } else {
                    getCallback(r);
                }
                ResetStatusAsync(t ? 1000 : 8000);
            };
        }
    }
}
