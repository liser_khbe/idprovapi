﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ipapilib;
using System;

namespace UTipapilib
{
    [TestClass]
    public class UTCustomer
    {
        [TestMethod]
        public void EqualsOnlyDefaults()
        {
            var a = new Customer("Durand", "Toto", "dur.to@email.com", AffiliationType.Student, "11223344");
            var b = new Customer("Durand", "Toto", "dur.to@email.com", AffiliationType.Student, "11223344");
            Assert.AreEqual(a, b);
            a.AffiliationType = AffiliationType.Teacher;
            Assert.AreNotEqual(a, b);
            a.AffiliationType = AffiliationType.Student;
            a.LastName = "Dupont";
            Assert.AreNotEqual(a, b);
        }
        [TestMethod]
        public void EqualsOptionals()
        {
            var a = Mock();
            var b = Mock();
                b.Login = "foo";
            Assert.AreEqual(a, b);
            a.Login = "bar";
            Assert.AreNotEqual(a, b);
            a.Login = "foo";
            Assert.AreEqual(a, b);
            a.CardUID = null;
            Assert.AreNotEqual(a, b);
        }
        [TestMethod]
        public void EqualsDateTime()
        {
            var a = Mock();
            a.DateOfBirth = new DateTime(2018, 3, 2);
            var b = Mock();
            b.DateOfBirth = DateTime.Parse("2018-03-02");
            Assert.AreEqual(a, b);
            a.DateOfBirth = DateTime.Parse("2018-03-01");
            b.DateOfBirth = new DateTime(2018, 3, 1);
            Assert.AreEqual(a, b);
            b.DateOfBirth = DateTime.Parse("2018-03-02");
            Assert.AreNotEqual(a, b);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidCardUID()
        {
            new Customer("Durand", "Toto", "dur.to@email.com", AffiliationType.Student, "112233");
        }
        [TestMethod]
        public void EqualsUID() {
            var a = Mock();
            var b = Mock();
            Assert.AreEqual(a, b);
            a.CardUID = null;
            Assert.AreNotEqual(a, b);
            a.CardUID = "";
            b.CardUID = null;
            Assert.AreNotEqual(a, b);
            a.CardUID = null;
            Assert.AreEqual(a, b);
        }
        private Customer Mock() 
            => new Customer("Durand", "Toto", "dur.to@email.com", AffiliationType.Student, "11223344");
    }
}
