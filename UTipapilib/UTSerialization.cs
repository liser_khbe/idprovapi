﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ipapilib;
using System.Collections.Generic;
using System;

namespace UTipapilib.Json
{
    [TestClass]
    public class CustomerConverter
    {
        [TestMethod]
        public void NullValueHandling()
        {
            var expected = "{\"CardUID\":\"11223344\",\"LastName\":\"foo\",\"FirstName\":\"bar\"" +
                ",\"EmailAddress\":\"email\",\"AffiliationType\":\"External\"}";
            var actual = JsonConvert.SerializeObject(new Customer("foo", "bar", "email", AffiliationType.External, "11223344"));
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void DateConversion() {
            var expected = "{\"CardUID\":\"11223344\",\"LastName\":\"foo\",\"FirstName\":\"bar\"" +
                ",\"EmailAddress\":\"email\",\"DateOfBirth\":\"1988-06-22\",\"AffiliationType\":\"External\"}";
            var actual = JsonConvert.SerializeObject(
                new Customer("foo", "bar", "email", AffiliationType.External, "11223344", dateOfBirth: new DateTime(1988, 06, 22)));
            Assert.AreEqual(expected, actual);
        }
    }
    [TestClass]
    public class StringEnumConverterTest
    {
        [TestMethod]
        public void Serialization()
        {
            var enummap = getEnummap();
            foreach(var expected in enummap)
            {
                var o = new MockObj { AffiliationType = expected.Value };
                var expectedString = $"{{\"AffiliationType\":\"{expected.Key}\"}}";
                var actualString = JsonConvert.SerializeObject(o);

                Assert.AreEqual(expectedString, actualString);
            }
        }
        [TestMethod]
        public void SerializationDate() {
            var o = new MockObj { Date = new DateTime(2018, 03, 01) };
            var expectedString = $"{{\"Date\":\"2018-03-01\"}}";
            var actualString = JsonConvert.SerializeObject(o);
            Assert.AreEqual(expectedString, actualString);
        }
        [TestMethod]
        public void Deserialization()
        {
            var enummap = getEnummap();
            foreach(var kv in enummap)
            {
                var json = $"{{ \"AffiliationType\" : \"{kv.Key}\" }}";
                var actual = JsonConvert.DeserializeObject<MockObj>(json);
                Assert.AreEqual(kv.Value, actual.AffiliationType);
            }

        }
        [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
        private class MockObj
        {
            public AffiliationType? AffiliationType { get; set; }
            [JsonConverter(typeof(CustomeDateTimeConverter))]
            public DateTime? Date { get; set; }
        }
        private Dictionary<string, AffiliationType> getEnummap()
        {
            return new Dictionary<string, AffiliationType>()
            {
                {"Student", AffiliationType.Student },
                {"Teacher", AffiliationType.Teacher },
                {"Staff", AffiliationType.Staff },
                {"External", AffiliationType.External },
            };
        }
    }
}
