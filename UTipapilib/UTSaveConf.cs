﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IdProvClient.IO;
using ipapilib.utils;

namespace UTipapilib.IO {
    /// <summary>
    /// SaveConf class Test
    /// </summary>
    [TestClass]
    public class UTSaveConf {
        private const string test_filename = "idprov_config_test.bin";
        public UTSaveConf() {
        }
        [TestMethod]
        public void EqualsTest() {
            var e = Mock();
            var a = Mock();
            a.Add("null", null);
            e.Add("null", null);
            Assert.IsTrue(Equals(a, e));
            /* test a has one more */
            e.Remove("null");
            Assert.IsFalse(Equals(a, e));
            a.Remove("null");
            Assert.IsTrue(Equals(a, e));
            /* test e has one more */
            e.Add("qux", "quux");
            Assert.IsFalse(Equals(a, e));
        }

        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void Load_FileNotFoundException() {
            var s = new SaveConf(test_filename);
            try {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + test_filename;
                System.IO.File.Delete(path);
            } catch (System.IO.FileNotFoundException) {}
            s.Load();
        }
        [TestMethod]
        public void SaveLoad_mock() {
            var s = new SaveConf(test_filename);
            var e = Mock();
            s.Save(e);
            var a = s.Load();

            Console.WriteLine("expected: " + e.ToPrettyString());
            Console.WriteLine("actual:   " + a.ToPrettyString());
            Assert.IsTrue(Equals(a, e));
        }
        [TestMethod]
        public void Convert_mock() {
            var e = Mock();

            var s = new SaveConf(test_filename);
            var a = SaveConf.Convert(MockStra());

            Console.WriteLine("expected: " + e.ToPrettyString());
            Console.WriteLine("actual:   " + a.ToPrettyString());
            Assert.IsTrue(Equals(e, a));
        }

        private static Dictionary<string, string> Mock() {
            return new Dictionary<string, string> {
                {"foo" , "bar"},
                {"baz" , "bah:blah" },
                {"dubs", ":checked" }
            };
        }
        private static string[] MockStra() {
            return new string[] {
                "foo:bar",
                "\t:quints",
                "akfhashfa;j",
                "   dubs:    :checked  \t  ",
                "baz:bah:blah"
            };
        }
        private static bool Equals<K,V>(Dictionary<K, V> a, Dictionary<K,V> b) {
            if (a.Count != b.Count)
                return false;
            foreach(var i in a) {
                if (!b.ContainsKey(i.Key))
                    return false;
                if (i.Value == null && b[i.Key] == null)
                    continue;
                if (i.Value == null && b[i.Key] != null
                    || i.Value != null && b[i.Key] == null
                    || !i.Value.Equals(b[i.Key]))
                    return false;
            }
            return true;
        }

        [ClassCleanup]
        public static void Cleanup() {
            try {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + test_filename;
                System.IO.File.Delete(path);
            } catch (System.IO.FileNotFoundException) {}
        }
    }
}
#region Additional test attributes
//
// You can use the following additional attributes as you write your tests:
//
// Use ClassInitialize to run code before running the first test in the class
// [ClassInitialize()]
// public static void MyClassInitialize(TestContext testContext) { }
//
// Use ClassCleanup to run code after all tests in a class have run
// [ClassCleanup()]
// public static void MyClassCleanup() { }
//
// Use TestInitialize to run code before running each test 
// [TestInitialize()]
// public void MyTestInitialize() { }
//
// Use TestCleanup to run code after each test has run
// [TestCleanup()]
// public void MyTestCleanup() { }
//
#endregion