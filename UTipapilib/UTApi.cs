﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ipapilib;
using ipapilib.Api;

namespace UTipapilib.Api
{
    [TestClass]
    public partial class UTApi
    {
        public static int cnt = 0;
        IdentityProvisioningAPI api;

        [TestInitialize]
        public void InitApi() 
        {
            api = new IdentityProvisioningAPI(new APIConfig(username, password));
        }
        [ClassInitialize]
        public static void CreateCustomer(TestContext context)
        {
            var expected = Mock();
            string elogin = expected.Login;
            expected.Login = null;
            var api = new IdentityProvisioningAPI(new APIConfig(username, password));
            string alogin = api.CreateCustomer(expected).Trim('"');
            Console.WriteLine(alogin);

            Assert.AreEqual(elogin, alogin);
            var actual = api.GetCustomerByLogin(alogin);
            Assert.AreEqual(expected, actual);
        }
        /* useless test if I'm gonna call GetCustomerByLogin in CreateCustomer's test */
        [TestMethod]
        public void getCustomerByLogin()
        {
            var expected = Mock();
            var actual = api.GetCustomerByLogin(expected.Login);
            Console.WriteLine($"exp:{expected}\nact:{actual}");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        [ExpectedException(typeof(ObjectNotFoundApiException))]
        public void getCustomerByUID_NotFoundException()
        {
            var expected = Mock();
            var actual = api.GetCustomerByUID(expected.CardUID);
            Assert.AreEqual(expected, actual);
            api.GetCustomerByUID("11111111");
        }
        [TestMethod]
        public void SetGetCustomerUID()
        {
            var expected = Mock();
            string testkey = "aabbccff";

            api.SetCustomerUID(expected.Login, testkey);
            var actual = api.GetCustomerByUID(testkey);
            Assert.AreNotEqual(expected, actual);
            api.SetCustomerUID(expected.Login, expected.CardUID);
            actual = api.GetCustomerByUID(expected.CardUID);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        [ExpectedException(typeof(ParamNotValidApiException))]
        public void SetCustomerUID_ParamException()
        {
            var expected = Mock();
            api.SetCustomerUID(expected.Login, "1122334455");
        }
        [TestMethod]
        public void UpdateCustomer()
        {
            var expected = Mock();
            expected.FirstName = "Tata";
            expected.DateOfBirth = DateTime.Parse("2018-01-01");
            api.UpdateCustomer(expected);
            var actual = api.GetCustomerByLogin(expected.Login);
            Assert.AreEqual(expected, actual);

            var original = Mock();
            api.UpdateCustomer(original);
            actual = api.GetCustomerByLogin(expected.Login);
            Assert.AreNotEqual(expected, actual);
            Assert.AreEqual(original, actual);
        }
        [TestMethod]
        [ExpectedException(typeof(DuplicateCustomerApiException))]
        public void UpdateCustomer_DuplicateEmailException()
        {
            var expected = Mock();
            api.CreateCustomer(new Customer("Dupont", "Toto", "dupont.toto@liser.lu", AffiliationType.Teacher, "11223344"));
            expected.EmailAddress = "dupont.toto@liser.lu";
            api.UpdateCustomer(expected);
        }
        [ClassCleanup]
        [ExpectedException(typeof(ObjectNotFoundApiException))]
        public static void DeleteCustomer()
        {
            var api = new IdentityProvisioningAPI(new APIConfig(username, password));
            var expected = Mock();
            api.DeleteCustomer(expected.Login);
            var actual = api.GetCustomerByLogin(expected.Login);
        }


        public static Customer Mock()
            => new Customer("Durand", "Toto", "durand.toto@liser.lu", 
                AffiliationType.Teacher, dateOfBirth: DateTime.Parse("0001-01-01"), cardUID: "aabbccdd",
                login: "DurTo32r");
    }
}
